REBAR=./rebar

all: clean deps compile

deps: get-deps

clean:
	$(REBAR) clean

compile:
	$(REBAR) compile

get-deps:
	$(REBAR) get-deps

update-deps:
	$(REBAR) update-deps

